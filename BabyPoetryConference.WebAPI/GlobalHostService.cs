﻿using BabyPoetryConference.Base;
using Microsoft.Extensions.Hosting;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BabyPoetryConference.WebAPI
{
    public class GlobalHostService : IHostedService
    {
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();
        private readonly IServiceManager _manager;

        public GlobalHostService(IServiceManager manager)
        {
            _manager = manager;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _Logger.Info("IHostedService-StartAsync:LoadCoreService...");
            _manager.LoadCoreService();
            return Task.Delay(5, cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                _manager.UnloadCoreService();
            }
            catch (Exception exception)
            {
                _Logger.Error(exception, "UnloadCoreService");
            }
            return Task.Delay(5, cancellationToken);
        }
    }
}
