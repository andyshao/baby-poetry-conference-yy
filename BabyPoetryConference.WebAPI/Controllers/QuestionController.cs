﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BabyPoetryConference.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace BabyPoetryConference.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class QuestionController : ControllerBase
    {
        private IQuestionService _QuestionService;
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();
        public QuestionController(IQuestionService questionService)
        {
            _QuestionService = questionService;
        }

        [HttpGet]
        public JsonResult generate()
        {
            var question = _QuestionService.GetQuestion();
            var questionView = new
            {
                Qustion = question.Content,
                Answer = question.Answer,
                Explain = string.IsNullOrEmpty(question.No) ? "没有解释可以显示给您" : question.Explain
        };
            return new JsonResult(questionView);
        }
    }
}
