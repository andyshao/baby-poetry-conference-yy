﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BabyPoetryConference.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WelcomeController : ControllerBase
    {
        public WelcomeController()
        {
            
        }

        [HttpGet]
        public String Get()
        {
            return "欢迎使用宝宝的诗词大会!";
        }
    }
}
