﻿using System.Collections.Generic;
using System.Text;

namespace BabyPoetryConference.Entities
{
    public class Poem
    {
        public string FullName { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Author { get; set; }
        public string Dynasty { get; set; }
        public int Recite { get; set; }
        public List<string> Sentences { get; set; }

        public Poem(string fullName, string title, string subTitle, string author, string dynasty, List<string> sentences)
        {
            this.FullName = fullName;
            this.Title = title;
            this.SubTitle = subTitle;
            this.Author = author;
            this.Dynasty = dynasty;
            this.Sentences = sentences;
        }

        /// <summary>
        /// 获取诗词内容
        /// </summary>
        /// <returns></returns>
        public string GetContent()
        {
            var content = new StringBuilder(this.FullName);
            var count = Sentences.Count;
            for (var i=0;i<count;i++)
            {
                if(i % 2 == 0)
                {
                    content.Append(Sentences[i]);
                }
                else
                {
                    content.Append(Sentences[i]).Append('\n');
                }
            }
            return content.ToString();
        }


    }
}
