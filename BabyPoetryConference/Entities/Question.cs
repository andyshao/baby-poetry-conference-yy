﻿using System;
using System.Collections.Generic;

namespace BabyPoetryConference
{
    public class Question
    {
        public string Subject { get; set; } = string.Empty;
        public string Content { get; set; } = string.Empty;
        public string Answer { get; set; } = string.Empty;
        public string Explain { get; set; } = string.Empty;
        public string No { get; set; } = string.Empty;
        public string BuildType { get; set; } = string.Empty;
    }
}
