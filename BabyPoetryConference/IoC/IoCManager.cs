﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using NLog;
using Module = Autofac.Module;

namespace BabyPoetryConference.IoC
{
    public class IoCManager
    {
        private static readonly ILogger _Logger = LogManager.GetCurrentClassLogger();

        #region Instance

        private IoCManager()
        {
        }
        public static IoCManager Instance { get; } = new IoCManager();

        #endregion

        public void Register(ContainerBuilder builder, params Module[] modules)
        {
            if (modules != null)
            {
                foreach (var module in modules)
                {
                    builder.RegisterModule(module);
                }
            }
            _Logger.Info($"Autofac Modules加载完成。");
        }
    }
}
