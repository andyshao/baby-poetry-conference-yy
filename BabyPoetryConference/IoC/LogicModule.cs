﻿using System;
using System.Collections.Generic;
using Autofac;
using System.Text;
using BabyPoetryConference.Base;
using BabyPoetryConference.DbHelpers;
using BabyPoetryConference.Services;

namespace BabyPoetryConference.IoC
{
    public class LogicModule : Module
    {
        #region Overrides of Module

        /// <inheritdoc />
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<Config>().AsSelf().SingleInstance();
            builder.RegisterType<ServiceManager>().As<IServiceManager>().SingleInstance();
            builder.RegisterType<QuestionService>().As<IQuestionService>().SingleInstance();
            builder.RegisterType<ToFileService>().AsSelf().SingleInstance();
            builder.RegisterType<PoetryDbHelperExcel>().AsSelf().SingleInstance();
        }

        #endregion
    }
}
