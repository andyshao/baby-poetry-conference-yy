﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace BabyPoetryConference
{
    public class Config
    {
        public IConfiguration Configuration { get; private set; }
        public Config(IConfiguration configuration)
        {
            Configuration = configuration;
        }
    }
}
