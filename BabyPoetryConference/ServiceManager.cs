﻿using BabyPoetryConference.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BabyPoetryConference
{
    public class ServiceManager : IServiceManager
    {
        private IQuestionService _QuestionService;
        public ServiceManager(IQuestionService questionService)
        {
            _QuestionService = questionService;
        }
        public void LoadCoreService()
        {
            _QuestionService.Start();
        }

        public void UnloadCoreService()
        {
            _QuestionService.Stop();
        }
    }
}
