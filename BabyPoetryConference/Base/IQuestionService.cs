﻿using System;
using System.Collections.Generic;
using System.Text;
using BabyPoetryConference.Entities;

namespace BabyPoetryConference.Base
{
    public interface IQuestionService : IService
    {
        string Msg { get; }
        Dictionary<string, Poem> PoetryDict { get; }
        Question GetQuestion();

        List<Question> GetAllQuestionsOfAllPoems();
    }
}
