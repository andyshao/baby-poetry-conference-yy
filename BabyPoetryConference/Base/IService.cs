﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BabyPoetryConference.Base
{
    public interface IService
    {
        void Start();

        void Stop();
    }
}
