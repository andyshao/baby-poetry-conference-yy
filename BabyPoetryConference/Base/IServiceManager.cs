﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BabyPoetryConference.Base
{
    public interface IServiceManager
    {
        /// <summary>
        ///     加载核心服务及插件
        /// </summary>  
        void LoadCoreService();
        /// <summary>
        ///     卸载核心服务及插件
        /// </summary>
        void UnloadCoreService();
    }
}
