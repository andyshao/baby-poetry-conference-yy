﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Autofac;
using Autofac.Configuration;
using BabyPoetryConference.IoC;
using BabyPoetryConference.WinUI.IoC;
using Microsoft.Extensions.Configuration;
using NLog;

namespace BabyPoetryConference.WinUI
{
    static class Program
    {
        internal static IContainer Container { get; private set; }

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            var builder = new ContainerBuilder();

            builder.Register(cc =>
            {
                //将配置添加到ConfigurationBuilder
                var config = new ConfigurationBuilder();
                //config.AddJsonFile来自Microsoft.Extensions.Configuration.Json
                //config.AddXmlFile来自Microsoft.Extensions.Configuration.Xml
                config.AddIniFile("cfg.ini");
                IConfiguration cfg = config.Build();
                return cfg;
            });

            IoCManager.Instance.Register(builder, GetPluginsModules());
            using (Container = builder.Build())
            {
                var startup = Container.Resolve<Startup>();
                //开启当前程序作用域下的 ApplicationContext 实例
                Application.Run(startup);
            }
        }

        private static Module[] GetPluginsModules()
        {
            //插件Module
            var list = new List<Module>();
            //WinForm客户端Module
            list.AddRange(new Module[] { new AppModule(), new LogicModule() });
            return list.ToArray();
        }
    }
}
