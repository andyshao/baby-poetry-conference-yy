﻿using BabyPoetryConference.Base;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BabyPoetryConference.WinUI
{
    public partial class ConferenceForm : Form
    {
        private Question _Question = new Question();
        private IQuestionService _QuestionService;
        private IConfiguration _Config;
        private int[,] _Scores;
        private Label[,] _ScoreLables;
        public ConferenceForm(IConfiguration config, IQuestionService questionService)
        {
            _Config = config;
            _QuestionService = questionService;
            _Scores = new int[6, 2];
            _ScoreLables = new Label[6, 2];
            InitializeComponent();

            _ScoreLables[0, 0] = RightCountLabel1;
            _ScoreLables[0, 1] = WrongCountLabel1;
            _ScoreLables[1, 0] = RightCountLabel2;
            _ScoreLables[1, 1] = WrongCountLabel2;
            _ScoreLables[2, 0] = RightCountLabel3;
            _ScoreLables[2, 1] = WrongCountLabel3;
            _ScoreLables[3, 0] = RightCountLabel4;
            _ScoreLables[3, 1] = WrongCountLabel4;
            _ScoreLables[4, 0] = RightCountLabel5;
            _ScoreLables[4, 1] = WrongCountLabel5;
            _ScoreLables[5, 0] = RightCountLabel6;
            _ScoreLables[5, 1] = WrongCountLabel6;

            _Question.Content = "胸藏文墨怀若谷,腹有诗书气自华。";
            _Question.Answer = _QuestionService.Msg;
        }

        

        /// <summary>
        /// 注释
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetExplainButton_Click(object sender, EventArgs e)
        {
            AnswerConentLabel.Text = string.IsNullOrEmpty(_Question.No) ? "没有解释可以显示给您" : _Question.Explain;
        }
        /// <summary>
        /// 答案
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetAnswerButton_Click(object sender, EventArgs e)
        {
            AnswerConentLabel.Text = _Question.Answer;
        }
        /// <summary>
        /// 下一题
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextQuestionButton_Click(object sender, EventArgs e)
        {
            //var no = _Config.GetSection("file").GetValue<string>("question_file");
             _Question = _QuestionService.GetQuestion();
            QuestionConentLabel.Text = _Question.Content;
            AnswerConentLabel.Text = "请作答";
        }

        private void AddRightButton1_Click(object sender, EventArgs e)
        {
            _ScoreLables[0, 0].Text = $"{++_Scores[0, 0]}";
        }

        private void AddWrongButton1_Click(object sender, EventArgs e)
        {
            _ScoreLables[0, 1].Text = $"{ ++_Scores[0, 1]}";
        }

        private void AddRightButton2_Click(object sender, EventArgs e)
        {
            _ScoreLables[1, 0].Text = $"{ ++_Scores[1, 0]}";
        }

        private void AddWrongButton2_Click(object sender, EventArgs e)
        {
            _ScoreLables[1, 1].Text = $"{ ++_Scores[1, 1]}";
        }

        private void AddRightButton3_Click(object sender, EventArgs e)
        {
            _ScoreLables[2, 0].Text = $"{ ++_Scores[2, 0]}";
        }

        private void AddWrongButton3_Click(object sender, EventArgs e)
        {
            _ScoreLables[2, 1].Text = $"{ ++_Scores[2, 1]}";
        }

        private void AddRightButton4_Click(object sender, EventArgs e)
        {
            _ScoreLables[3, 0].Text = $"{ ++_Scores[3, 0]}";
        }

        private void AddWrongButton4_Click(object sender, EventArgs e)
        {
            _ScoreLables[3, 1].Text = $"{ ++_Scores[3, 1]}";
        }

        private void AddRightButton5_Click(object sender, EventArgs e)
        {
            _ScoreLables[4, 0].Text = $"{ ++_Scores[4, 0]}";
        }

        private void AddWrongButton5_Click(object sender, EventArgs e)
        {
            _ScoreLables[4, 1].Text = $"{ ++_Scores[4, 1]}";
        }

        private void AddRightButton6_Click(object sender, EventArgs e)
        {
            _ScoreLables[5, 0].Text = $"{ ++_Scores[5, 0]}";
        }

        private void AddWrongButton6_Click(object sender, EventArgs e)
        {
            _ScoreLables[5, 1].Text = $"{ ++_Scores[5, 1]}";
        }

        private void ClearCountButton_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < 6; i++)
            {
                for(int j=0;j < 2;j++)
                {
                    _Scores[i, j] = 0;
                    _ScoreLables[i, j].Text = "0";
                }
            }
        }

        private void ConferenceForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }
    }
}
