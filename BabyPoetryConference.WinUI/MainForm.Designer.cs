﻿namespace BabyPoetryConference.WinUI
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.ConferenceButon = new System.Windows.Forms.Button();
            this.PaperButton = new System.Windows.Forms.Button();
            this.DictionaryButton = new System.Windows.Forms.Button();
            this.RepositoryButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ConferenceButon
            // 
            this.ConferenceButon.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ConferenceButon.Location = new System.Drawing.Point(12, 22);
            this.ConferenceButon.Name = "ConferenceButon";
            this.ConferenceButon.Size = new System.Drawing.Size(264, 41);
            this.ConferenceButon.TabIndex = 0;
            this.ConferenceButon.Text = "诗词大会";
            this.ConferenceButon.UseVisualStyleBackColor = true;
            this.ConferenceButon.Click += new System.EventHandler(this.ConferenceButon_Click);
            // 
            // PaperButton
            // 
            this.PaperButton.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PaperButton.Location = new System.Drawing.Point(12, 82);
            this.PaperButton.Name = "PaperButton";
            this.PaperButton.Size = new System.Drawing.Size(264, 41);
            this.PaperButton.TabIndex = 1;
            this.PaperButton.Text = "诗词试卷";
            this.PaperButton.UseVisualStyleBackColor = true;
            this.PaperButton.Click += new System.EventHandler(this.PaperButton_Click);
            // 
            // DictionaryButton
            // 
            this.DictionaryButton.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.DictionaryButton.Location = new System.Drawing.Point(12, 144);
            this.DictionaryButton.Name = "DictionaryButton";
            this.DictionaryButton.Size = new System.Drawing.Size(264, 41);
            this.DictionaryButton.TabIndex = 2;
            this.DictionaryButton.Text = "诗词列表";
            this.DictionaryButton.UseVisualStyleBackColor = true;
            this.DictionaryButton.Click += new System.EventHandler(this.DictionaryButton_Click);
            // 
            // RepositoryButton
            // 
            this.RepositoryButton.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.RepositoryButton.Location = new System.Drawing.Point(12, 202);
            this.RepositoryButton.Name = "RepositoryButton";
            this.RepositoryButton.Size = new System.Drawing.Size(264, 41);
            this.RepositoryButton.TabIndex = 3;
            this.RepositoryButton.Text = "生成题库";
            this.RepositoryButton.UseVisualStyleBackColor = true;
            this.RepositoryButton.Click += new System.EventHandler(this.RepositoryButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ExitButton.Location = new System.Drawing.Point(12, 310);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(264, 41);
            this.ExitButton.TabIndex = 4;
            this.ExitButton.Text = "退出";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 363);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.RepositoryButton);
            this.Controls.Add(this.DictionaryButton);
            this.Controls.Add(this.PaperButton);
            this.Controls.Add(this.ConferenceButon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "宝宝的诗词大会";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ConferenceButon;
        private System.Windows.Forms.Button PaperButton;
        private System.Windows.Forms.Button DictionaryButton;
        private System.Windows.Forms.Button RepositoryButton;
        private System.Windows.Forms.Button ExitButton;
    }
}

