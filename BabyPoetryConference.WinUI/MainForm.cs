﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BabyPoetryConference.Extensions;
using BabyPoetryConference.Services;

namespace BabyPoetryConference.WinUI
{
    public partial class MainForm : Form
    {
        private IConfiguration _Config;
        private ConferenceForm _ConferenceForm;
        private ToFileService _ToFileService;
        public MainForm(IConfiguration config, ConferenceForm conferenceForm, ToFileService toFileService)
        {
            _Config = config;
            _ConferenceForm = conferenceForm;
            _ToFileService = toFileService;
            InitializeComponent();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            _ConferenceForm.Dispose(); //此处需要显式释放窗体
            this.Close();
        }

        private void ConferenceButon_Click(object sender, EventArgs e)
        {
            _ConferenceForm.Show();
        }
        /// <summary>
        /// 诗词试卷
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PaperButton_Click(object sender, EventArgs e)
        {
            var msg = "生成以下试卷到D盘：\n";
            var filesCount = _Config.GetSection("to_word").GetInt("files_count");
            for(var i = 0; i < filesCount; i++)
            {
                var filenameQuestion = _ToFileService.QuestionToWord($"{i+1}");
                var filenameAnswer = _ToFileService.AnswerToWord($"{i + 1}");
                msg = $"{msg}\n{filenameQuestion}\n{filenameAnswer}";
            }
            MessageBox.Show(msg);
        }
        /// <summary>
        /// 诗词列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DictionaryButton_Click(object sender, EventArgs e)
        {
            var fileName = _ToFileService.PoemsToWord();
            var msg = $"生成以下试卷到D盘：\n\n{fileName}"; 
            MessageBox.Show(msg);
        }
        /// <summary>
        /// 生成题库,把所有问题输出到Excel表格中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RepositoryButton_Click(object sender, EventArgs e)
        {
            int count;
            var fileName = _ToFileService.AllQuestionsOfAllPoemsToExcel(out count);
            var msg = $"当前共【{count}】个问题\n生成以下文件到D盘：\n【{fileName}】";
            MessageBox.Show(msg);
        }
    }
}
