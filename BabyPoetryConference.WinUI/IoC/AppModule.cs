﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Microsoft.Extensions.Configuration;

namespace BabyPoetryConference.WinUI.IoC
{
    public class AppModule : Module
    {
        #region Overrides of Module

        /// <inheritdoc />
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<Startup>().AsSelf().SingleInstance();
            builder.RegisterType<MainForm>().AsSelf().SingleInstance();
            builder.RegisterType<ConferenceForm>().AsSelf();
        }

        #endregion
    }
}
