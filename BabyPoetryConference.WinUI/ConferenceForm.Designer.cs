﻿namespace BabyPoetryConference.WinUI
{
    partial class ConferenceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.QuestionConentLabel = new System.Windows.Forms.Label();
            this.AnswerConentLabel = new System.Windows.Forms.Label();
            this.GetExplainButton = new System.Windows.Forms.Button();
            this.GetAnswerButton = new System.Windows.Forms.Button();
            this.NextQuestionButton = new System.Windows.Forms.Button();
            this.ClearCountButton = new System.Windows.Forms.Button();
            this.AddRightButton1 = new System.Windows.Forms.Button();
            this.AddWrongButton1 = new System.Windows.Forms.Button();
            this.RightCountLabel1 = new System.Windows.Forms.Label();
            this.WrongCountLabel1 = new System.Windows.Forms.Label();
            this.WrongCountLabel2 = new System.Windows.Forms.Label();
            this.RightCountLabel2 = new System.Windows.Forms.Label();
            this.AddWrongButton2 = new System.Windows.Forms.Button();
            this.AddRightButton2 = new System.Windows.Forms.Button();
            this.WrongCountLabel3 = new System.Windows.Forms.Label();
            this.RightCountLabel3 = new System.Windows.Forms.Label();
            this.AddWrongButton3 = new System.Windows.Forms.Button();
            this.AddRightButton3 = new System.Windows.Forms.Button();
            this.WrongCountLabel4 = new System.Windows.Forms.Label();
            this.RightCountLabel4 = new System.Windows.Forms.Label();
            this.AddWrongButton4 = new System.Windows.Forms.Button();
            this.AddRightButton4 = new System.Windows.Forms.Button();
            this.WrongCountLabel5 = new System.Windows.Forms.Label();
            this.RightCountLabel5 = new System.Windows.Forms.Label();
            this.AddWrongButton5 = new System.Windows.Forms.Button();
            this.AddRightButton5 = new System.Windows.Forms.Button();
            this.WrongCountLabel6 = new System.Windows.Forms.Label();
            this.RightCountLabel6 = new System.Windows.Forms.Label();
            this.AddWrongButton6 = new System.Windows.Forms.Button();
            this.AddRightButton6 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // QuestionConentLabel
            // 
            this.QuestionConentLabel.Font = new System.Drawing.Font("楷体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.QuestionConentLabel.Location = new System.Drawing.Point(49, 52);
            this.QuestionConentLabel.Name = "QuestionConentLabel";
            this.QuestionConentLabel.Size = new System.Drawing.Size(835, 82);
            this.QuestionConentLabel.TabIndex = 0;
            this.QuestionConentLabel.Text = "胸藏文墨怀若谷,腹有诗书气自华。";
            this.QuestionConentLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // AnswerConentLabel
            // 
            this.AnswerConentLabel.Font = new System.Drawing.Font("楷体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.AnswerConentLabel.Location = new System.Drawing.Point(45, 134);
            this.AnswerConentLabel.Name = "AnswerConentLabel";
            this.AnswerConentLabel.Size = new System.Drawing.Size(839, 283);
            this.AnswerConentLabel.TabIndex = 1;
            this.AnswerConentLabel.Text = "欢迎挑战！";
            this.AnswerConentLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // GetExplainButton
            // 
            this.GetExplainButton.Location = new System.Drawing.Point(9, 435);
            this.GetExplainButton.Name = "GetExplainButton";
            this.GetExplainButton.Size = new System.Drawing.Size(300, 23);
            this.GetExplainButton.TabIndex = 2;
            this.GetExplainButton.Text = "注释";
            this.GetExplainButton.UseVisualStyleBackColor = true;
            this.GetExplainButton.Click += new System.EventHandler(this.GetExplainButton_Click);
            // 
            // GetAnswerButton
            // 
            this.GetAnswerButton.Location = new System.Drawing.Point(316, 435);
            this.GetAnswerButton.Name = "GetAnswerButton";
            this.GetAnswerButton.Size = new System.Drawing.Size(300, 23);
            this.GetAnswerButton.TabIndex = 3;
            this.GetAnswerButton.Text = "答案";
            this.GetAnswerButton.UseVisualStyleBackColor = true;
            this.GetAnswerButton.Click += new System.EventHandler(this.GetAnswerButton_Click);
            // 
            // NextQuestionButton
            // 
            this.NextQuestionButton.Location = new System.Drawing.Point(623, 435);
            this.NextQuestionButton.Name = "NextQuestionButton";
            this.NextQuestionButton.Size = new System.Drawing.Size(300, 23);
            this.NextQuestionButton.TabIndex = 4;
            this.NextQuestionButton.Text = "下一题";
            this.NextQuestionButton.UseVisualStyleBackColor = true;
            this.NextQuestionButton.Click += new System.EventHandler(this.NextQuestionButton_Click);
            // 
            // ClearCountButton
            // 
            this.ClearCountButton.Location = new System.Drawing.Point(9, 477);
            this.ClearCountButton.Name = "ClearCountButton";
            this.ClearCountButton.Size = new System.Drawing.Size(30, 91);
            this.ClearCountButton.TabIndex = 5;
            this.ClearCountButton.Text = "清  零";
            this.ClearCountButton.UseVisualStyleBackColor = true;
            this.ClearCountButton.Click += new System.EventHandler(this.ClearCountButton_Click);
            // 
            // AddRightButton1
            // 
            this.AddRightButton1.Location = new System.Drawing.Point(119, 513);
            this.AddRightButton1.Name = "AddRightButton1";
            this.AddRightButton1.Size = new System.Drawing.Size(68, 22);
            this.AddRightButton1.TabIndex = 6;
            this.AddRightButton1.Text = "正确";
            this.AddRightButton1.UseVisualStyleBackColor = true;
            this.AddRightButton1.Click += new System.EventHandler(this.AddRightButton1_Click);
            // 
            // AddWrongButton1
            // 
            this.AddWrongButton1.Location = new System.Drawing.Point(119, 546);
            this.AddWrongButton1.Name = "AddWrongButton1";
            this.AddWrongButton1.Size = new System.Drawing.Size(68, 22);
            this.AddWrongButton1.TabIndex = 7;
            this.AddWrongButton1.Text = "错误";
            this.AddWrongButton1.UseVisualStyleBackColor = true;
            this.AddWrongButton1.Click += new System.EventHandler(this.AddWrongButton1_Click);
            // 
            // RightCountLabel1
            // 
            this.RightCountLabel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RightCountLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RightCountLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RightCountLabel1.Location = new System.Drawing.Point(45, 513);
            this.RightCountLabel1.Name = "RightCountLabel1";
            this.RightCountLabel1.Size = new System.Drawing.Size(68, 22);
            this.RightCountLabel1.TabIndex = 8;
            this.RightCountLabel1.Text = "0";
            this.RightCountLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WrongCountLabel1
            // 
            this.WrongCountLabel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.WrongCountLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WrongCountLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.WrongCountLabel1.Location = new System.Drawing.Point(45, 546);
            this.WrongCountLabel1.Name = "WrongCountLabel1";
            this.WrongCountLabel1.Size = new System.Drawing.Size(68, 22);
            this.WrongCountLabel1.TabIndex = 9;
            this.WrongCountLabel1.Text = "0";
            this.WrongCountLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WrongCountLabel2
            // 
            this.WrongCountLabel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.WrongCountLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WrongCountLabel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.WrongCountLabel2.Location = new System.Drawing.Point(193, 546);
            this.WrongCountLabel2.Name = "WrongCountLabel2";
            this.WrongCountLabel2.Size = new System.Drawing.Size(68, 22);
            this.WrongCountLabel2.TabIndex = 14;
            this.WrongCountLabel2.Text = "0";
            this.WrongCountLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RightCountLabel2
            // 
            this.RightCountLabel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RightCountLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RightCountLabel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RightCountLabel2.Location = new System.Drawing.Point(193, 513);
            this.RightCountLabel2.Name = "RightCountLabel2";
            this.RightCountLabel2.Size = new System.Drawing.Size(68, 22);
            this.RightCountLabel2.TabIndex = 13;
            this.RightCountLabel2.Text = "0";
            this.RightCountLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddWrongButton2
            // 
            this.AddWrongButton2.Location = new System.Drawing.Point(267, 546);
            this.AddWrongButton2.Name = "AddWrongButton2";
            this.AddWrongButton2.Size = new System.Drawing.Size(68, 22);
            this.AddWrongButton2.TabIndex = 12;
            this.AddWrongButton2.Text = "错误";
            this.AddWrongButton2.UseVisualStyleBackColor = true;
            this.AddWrongButton2.Click += new System.EventHandler(this.AddWrongButton2_Click);
            // 
            // AddRightButton2
            // 
            this.AddRightButton2.Location = new System.Drawing.Point(267, 513);
            this.AddRightButton2.Name = "AddRightButton2";
            this.AddRightButton2.Size = new System.Drawing.Size(68, 22);
            this.AddRightButton2.TabIndex = 11;
            this.AddRightButton2.Text = "正确";
            this.AddRightButton2.UseVisualStyleBackColor = true;
            this.AddRightButton2.Click += new System.EventHandler(this.AddRightButton2_Click);
            // 
            // WrongCountLabel3
            // 
            this.WrongCountLabel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.WrongCountLabel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WrongCountLabel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.WrongCountLabel3.Location = new System.Drawing.Point(341, 546);
            this.WrongCountLabel3.Name = "WrongCountLabel3";
            this.WrongCountLabel3.Size = new System.Drawing.Size(68, 22);
            this.WrongCountLabel3.TabIndex = 19;
            this.WrongCountLabel3.Text = "0";
            this.WrongCountLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RightCountLabel3
            // 
            this.RightCountLabel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RightCountLabel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RightCountLabel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RightCountLabel3.Location = new System.Drawing.Point(341, 513);
            this.RightCountLabel3.Name = "RightCountLabel3";
            this.RightCountLabel3.Size = new System.Drawing.Size(68, 22);
            this.RightCountLabel3.TabIndex = 18;
            this.RightCountLabel3.Text = "0";
            this.RightCountLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddWrongButton3
            // 
            this.AddWrongButton3.Location = new System.Drawing.Point(415, 546);
            this.AddWrongButton3.Name = "AddWrongButton3";
            this.AddWrongButton3.Size = new System.Drawing.Size(68, 22);
            this.AddWrongButton3.TabIndex = 17;
            this.AddWrongButton3.Text = "错误";
            this.AddWrongButton3.UseVisualStyleBackColor = true;
            this.AddWrongButton3.Click += new System.EventHandler(this.AddWrongButton3_Click);
            // 
            // AddRightButton3
            // 
            this.AddRightButton3.Location = new System.Drawing.Point(415, 513);
            this.AddRightButton3.Name = "AddRightButton3";
            this.AddRightButton3.Size = new System.Drawing.Size(68, 22);
            this.AddRightButton3.TabIndex = 16;
            this.AddRightButton3.Text = "正确";
            this.AddRightButton3.UseVisualStyleBackColor = true;
            this.AddRightButton3.Click += new System.EventHandler(this.AddRightButton3_Click);
            // 
            // WrongCountLabel4
            // 
            this.WrongCountLabel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.WrongCountLabel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WrongCountLabel4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.WrongCountLabel4.Location = new System.Drawing.Point(489, 546);
            this.WrongCountLabel4.Name = "WrongCountLabel4";
            this.WrongCountLabel4.Size = new System.Drawing.Size(68, 22);
            this.WrongCountLabel4.TabIndex = 24;
            this.WrongCountLabel4.Text = "0";
            this.WrongCountLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RightCountLabel4
            // 
            this.RightCountLabel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RightCountLabel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RightCountLabel4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RightCountLabel4.Location = new System.Drawing.Point(489, 513);
            this.RightCountLabel4.Name = "RightCountLabel4";
            this.RightCountLabel4.Size = new System.Drawing.Size(68, 22);
            this.RightCountLabel4.TabIndex = 23;
            this.RightCountLabel4.Text = "0";
            this.RightCountLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddWrongButton4
            // 
            this.AddWrongButton4.Location = new System.Drawing.Point(563, 546);
            this.AddWrongButton4.Name = "AddWrongButton4";
            this.AddWrongButton4.Size = new System.Drawing.Size(68, 22);
            this.AddWrongButton4.TabIndex = 22;
            this.AddWrongButton4.Text = "错误";
            this.AddWrongButton4.UseVisualStyleBackColor = true;
            this.AddWrongButton4.Click += new System.EventHandler(this.AddWrongButton4_Click);
            // 
            // AddRightButton4
            // 
            this.AddRightButton4.Location = new System.Drawing.Point(563, 513);
            this.AddRightButton4.Name = "AddRightButton4";
            this.AddRightButton4.Size = new System.Drawing.Size(68, 22);
            this.AddRightButton4.TabIndex = 21;
            this.AddRightButton4.Text = "正确";
            this.AddRightButton4.UseVisualStyleBackColor = true;
            this.AddRightButton4.Click += new System.EventHandler(this.AddRightButton4_Click);
            // 
            // WrongCountLabel5
            // 
            this.WrongCountLabel5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.WrongCountLabel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WrongCountLabel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.WrongCountLabel5.Location = new System.Drawing.Point(637, 546);
            this.WrongCountLabel5.Name = "WrongCountLabel5";
            this.WrongCountLabel5.Size = new System.Drawing.Size(68, 22);
            this.WrongCountLabel5.TabIndex = 29;
            this.WrongCountLabel5.Text = "0";
            this.WrongCountLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RightCountLabel5
            // 
            this.RightCountLabel5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RightCountLabel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RightCountLabel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RightCountLabel5.Location = new System.Drawing.Point(637, 513);
            this.RightCountLabel5.Name = "RightCountLabel5";
            this.RightCountLabel5.Size = new System.Drawing.Size(68, 22);
            this.RightCountLabel5.TabIndex = 28;
            this.RightCountLabel5.Text = "0";
            this.RightCountLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddWrongButton5
            // 
            this.AddWrongButton5.Location = new System.Drawing.Point(711, 546);
            this.AddWrongButton5.Name = "AddWrongButton5";
            this.AddWrongButton5.Size = new System.Drawing.Size(68, 22);
            this.AddWrongButton5.TabIndex = 27;
            this.AddWrongButton5.Text = "错误";
            this.AddWrongButton5.UseVisualStyleBackColor = true;
            this.AddWrongButton5.Click += new System.EventHandler(this.AddWrongButton5_Click);
            // 
            // AddRightButton5
            // 
            this.AddRightButton5.Location = new System.Drawing.Point(711, 513);
            this.AddRightButton5.Name = "AddRightButton5";
            this.AddRightButton5.Size = new System.Drawing.Size(68, 22);
            this.AddRightButton5.TabIndex = 26;
            this.AddRightButton5.Text = "正确";
            this.AddRightButton5.UseVisualStyleBackColor = true;
            this.AddRightButton5.Click += new System.EventHandler(this.AddRightButton5_Click);
            // 
            // WrongCountLabel6
            // 
            this.WrongCountLabel6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.WrongCountLabel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WrongCountLabel6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.WrongCountLabel6.Location = new System.Drawing.Point(785, 546);
            this.WrongCountLabel6.Name = "WrongCountLabel6";
            this.WrongCountLabel6.Size = new System.Drawing.Size(68, 22);
            this.WrongCountLabel6.TabIndex = 34;
            this.WrongCountLabel6.Text = "0";
            this.WrongCountLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RightCountLabel6
            // 
            this.RightCountLabel6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.RightCountLabel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RightCountLabel6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RightCountLabel6.Location = new System.Drawing.Point(785, 513);
            this.RightCountLabel6.Name = "RightCountLabel6";
            this.RightCountLabel6.Size = new System.Drawing.Size(68, 22);
            this.RightCountLabel6.TabIndex = 33;
            this.RightCountLabel6.Text = "0";
            this.RightCountLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddWrongButton6
            // 
            this.AddWrongButton6.Location = new System.Drawing.Point(859, 546);
            this.AddWrongButton6.Name = "AddWrongButton6";
            this.AddWrongButton6.Size = new System.Drawing.Size(68, 22);
            this.AddWrongButton6.TabIndex = 32;
            this.AddWrongButton6.Text = "错误";
            this.AddWrongButton6.UseVisualStyleBackColor = true;
            this.AddWrongButton6.Click += new System.EventHandler(this.AddWrongButton6_Click);
            // 
            // AddRightButton6
            // 
            this.AddRightButton6.Location = new System.Drawing.Point(859, 513);
            this.AddRightButton6.Name = "AddRightButton6";
            this.AddRightButton6.Size = new System.Drawing.Size(68, 22);
            this.AddRightButton6.TabIndex = 31;
            this.AddRightButton6.Text = "正确";
            this.AddRightButton6.UseVisualStyleBackColor = true;
            this.AddRightButton6.Click += new System.EventHandler(this.AddRightButton6_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.Location = new System.Drawing.Point(45, 477);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(142, 26);
            this.textBox1.TabIndex = 36;
            this.textBox1.Text = "选手1";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox2.Location = new System.Drawing.Point(193, 477);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(142, 26);
            this.textBox2.TabIndex = 37;
            this.textBox2.Text = "选手2";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox3.Location = new System.Drawing.Point(341, 477);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(142, 26);
            this.textBox3.TabIndex = 38;
            this.textBox3.Text = "选手3";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox4.Location = new System.Drawing.Point(489, 477);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(142, 26);
            this.textBox4.TabIndex = 39;
            this.textBox4.Text = "选手4";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox5.Location = new System.Drawing.Point(637, 477);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(142, 26);
            this.textBox5.TabIndex = 40;
            this.textBox5.Text = "选手5";
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox6.Location = new System.Drawing.Point(785, 477);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(142, 26);
            this.textBox6.TabIndex = 41;
            this.textBox6.Text = "选手6";
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ConferenceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 581);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.WrongCountLabel6);
            this.Controls.Add(this.RightCountLabel6);
            this.Controls.Add(this.AddWrongButton6);
            this.Controls.Add(this.AddRightButton6);
            this.Controls.Add(this.WrongCountLabel5);
            this.Controls.Add(this.RightCountLabel5);
            this.Controls.Add(this.AddWrongButton5);
            this.Controls.Add(this.AddRightButton5);
            this.Controls.Add(this.WrongCountLabel4);
            this.Controls.Add(this.RightCountLabel4);
            this.Controls.Add(this.AddWrongButton4);
            this.Controls.Add(this.AddRightButton4);
            this.Controls.Add(this.WrongCountLabel3);
            this.Controls.Add(this.RightCountLabel3);
            this.Controls.Add(this.AddWrongButton3);
            this.Controls.Add(this.AddRightButton3);
            this.Controls.Add(this.WrongCountLabel2);
            this.Controls.Add(this.RightCountLabel2);
            this.Controls.Add(this.AddWrongButton2);
            this.Controls.Add(this.AddRightButton2);
            this.Controls.Add(this.WrongCountLabel1);
            this.Controls.Add(this.RightCountLabel1);
            this.Controls.Add(this.AddWrongButton1);
            this.Controls.Add(this.AddRightButton1);
            this.Controls.Add(this.ClearCountButton);
            this.Controls.Add(this.NextQuestionButton);
            this.Controls.Add(this.GetAnswerButton);
            this.Controls.Add(this.GetExplainButton);
            this.Controls.Add(this.AnswerConentLabel);
            this.Controls.Add(this.QuestionConentLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ConferenceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "宝宝的诗词大会";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConferenceForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label QuestionConentLabel;
        private System.Windows.Forms.Label AnswerConentLabel;
        private System.Windows.Forms.Button GetExplainButton;
        private System.Windows.Forms.Button GetAnswerButton;
        private System.Windows.Forms.Button NextQuestionButton;
        private System.Windows.Forms.Button ClearCountButton;
        private System.Windows.Forms.Button AddRightButton1;
        private System.Windows.Forms.Button AddWrongButton1;
        private System.Windows.Forms.Label RightCountLabel1;
        private System.Windows.Forms.Label WrongCountLabel1;
        private System.Windows.Forms.Label WrongCountLabel2;
        private System.Windows.Forms.Label RightCountLabel2;
        private System.Windows.Forms.Button AddWrongButton2;
        private System.Windows.Forms.Button AddRightButton2;
        private System.Windows.Forms.Label WrongCountLabel3;
        private System.Windows.Forms.Label RightCountLabel3;
        private System.Windows.Forms.Button AddWrongButton3;
        private System.Windows.Forms.Button AddRightButton3;
        private System.Windows.Forms.Label WrongCountLabel4;
        private System.Windows.Forms.Label RightCountLabel4;
        private System.Windows.Forms.Button AddWrongButton4;
        private System.Windows.Forms.Button AddRightButton4;
        private System.Windows.Forms.Label WrongCountLabel5;
        private System.Windows.Forms.Label RightCountLabel5;
        private System.Windows.Forms.Button AddWrongButton5;
        private System.Windows.Forms.Button AddRightButton5;
        private System.Windows.Forms.Label WrongCountLabel6;
        private System.Windows.Forms.Label RightCountLabel6;
        private System.Windows.Forms.Button AddWrongButton6;
        private System.Windows.Forms.Button AddRightButton6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
    }
}